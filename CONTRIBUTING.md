# Contributing

When contributing to this repository,
please first discuss the change you wish to make via issue with the owners of this repository
before making a change.

When you think the code is ready for review
a pull/merge request should be created.
Owners of the repository will watch out for new pull/merge requests
and
review them in regular intervals.
If comments have been given in a review,
they have to get integrated.
For those changes a separate commit should be created
and
pushed to your remote development branch.
Keep in mind that reviews can span multiple cycles until the owners are happy with the new code.

## References

They should be updated first in the shared Zotero.