## Summary

(Summarize the bug encountered concisely)

## Checklist

- [ ] Document has 6 or less figure.
- [ ] Figures are combined into one text document in the order they appear in the text, with figure legends immediately following the relevant figure.
- [ ] Figures are cropped to area of interest.
- [ ] Figures are numbered. For example, figure 1a, figure 1b, figure 2.
- [ ] Figures has no more than four components (a, b, c, d).
- [ ] Figure has label for all features described in the figure legend.
- [ ] No patient and subject identifying information is available in the figure.
- [ ] If a figure has been enhanced electronically, explain the alterations made and send an original image along with the enhanced one.
