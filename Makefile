.PHONY: _book final references-from-zotero.bib img

ALL_RMD := $(wildcard *.Rmd)

all: html html_book

html: article.html

html_book: _book

docx: article.docx

_book: $(ALL_RMD) references-from-zotero.bib img
	Rscript -e 'bookdown::render_book(".", "bookdown::gitbook")'

article.html: $(ALL_RMD) references-from-zotero.bib img
	Rscript -e 'bookdown::render_book(".", "bookdown::html_document2")'

article.docx: $(ALL_RMD) references-from-zotero.bib img
	Rscript -e 'bookdown::render_book(".", "bookdown::word_document2")'

img:
	$(MAKE) -C $@

references-from-zotero.bib:
	@echo 'Use Better BibTeX for Zotero <https://retorque.re/zotero-better-bibtex> to keep keys updated.'

wc:
	@echo 'Abstract: 250 words or less'
	@Rscript -e 'wordcountaddin::word_count("index.Rmd")' 2>/dev/null
	@echo 'Introduction: 400 words or less'
	@Rscript -e 'wordcountaddin::word_count("01-introduction.Rmd")' 2>/dev/null
	@echo 'Materials and Methods: 800 words or less'
	@Rscript -e 'wordcountaddin::word_count("02-materials-and-methods.Rmd")' 2>/dev/null
	@echo 'Results: 1000 words or less'
	@Rscript -e 'wordcountaddin::word_count("03-results.Rmd")' 2>/dev/null
	@echo 'Discussion: 800 words or less'
	@Rscript -e 'wordcountaddin::word_count("04-discussion.Rmd")' 2>/dev/null
